import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { loadState } from './localStorage'
import reducers from '../reducers'

const configureStore = () => {
  const persistedState = loadState()
  const middlewares = [thunk]
  let enhancer

  enhancer = applyMiddleware(...middlewares)

  return createStore(
    reducers,
    persistedState,
    enhancer
  )
}

export default configureStore
