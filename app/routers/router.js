import React from 'react';
import { StackNavigator } from 'react-navigation';

import styles from './StackNavigator.style'
import HomePage from '../pages/HomePage/HomePage';
import DetailImage from '../pages/HomePage/DetailImagePage';

export default StackNavigator({
  HomePage: {
    screen: HomePage,
    navigationOptions: ({ navigation }) => ({
      title: 'Gallery',
      headerTintColor: styles.headerTintColor,
      headerStyle: styles.headerStyle,
    })
  },
  DetailPage: {
    screen: DetailImage,
    navigationOptions: ({ navigation }) => ({
      title: navigation.state.params.name,
      headerTintColor: styles.headerTintColor,
      headerStyle: styles.headerStyle
    })
  }
});
