export default {
  headerTintColor: "#ffffff",
  headerStyle: {
    backgroundColor: '#3A4255',
    borderBottomColor: '#d63031',
    borderBottomWidth: 3,
    zIndex: 10
  }
};
