import { API_URL, API_KEY } from '../constants'
import axios from 'axios';

export function getImages(pageNum) {
  return (dispatch) => {
    dispatch({ type: 'LOAD_GALLERY' })

    const page = pageNum || 1

    axios.get(`${API_URL}&page=${page}`)
      .then(response => (
        dispatch({
          type: 'LOAD_GALLERY_SUCCESS',
          data: response.data.photos,
          currentPage: page
        })
      ))
      .catch((err) =>(
        dispatch({
          type: 'LOAD_GALLERY_FAIL'
        })
      ))
  }
}
