import { Dimensions } from 'react-native'

const dimensions = Dimensions.get('window');
const modalMarginTop = Math.round(dimensions.height/2 - 100);

export default {
  container: {
    flex: 1,
    backgroundColor: '#f1f2f6',
  },
  loaderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 40
  },
  loader: {
    width: 150,
    height: 150
  },
  list: {
    padding: 5
  },
  imageItem: {
    flex: 1,
    margin: 5,
    height: 170
  },
  imagePreview: {
    width: '100%',
    height: 170
  },
  innerContainer: {
    flex: 1,
    backgroundColor: '#000000',
  },
  imageInnerContainer: {
    flex: 1,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#000000'
  },
  imageInner: {
    width: '100%',
    height: 350
  },
  overlayContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 10,
    paddingTop: 10
  },
  overlayAdditionalInfo: {
    width: '85%',
  },
  overlayTitle: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: '800'
  },
  overlayMoreInfo: {
    color: '#ffffff',
    fontSize: 14
  },
  shareButtonContainer: {
    width: '15%',
    alignItems:'right',
  },
  shareButton: {
    backgroundColor: "#d63031",
    borderRadius: 20,
    alignItems:'center',
    justifyContent:'center',
    width: 40,
    height: 40
  },
  shareModalWindow: {
    padding: 20,
    margin: 20,
    marginTop: modalMarginTop,
    backgroundColor: '#f1f2f6'
  },
  closeModalButtonContainer: {
    position: 'absolute',
    top: -20,
    right: -10,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems:'center',
    justifyContent:'center'
  },
  shareModalWindowTitle: {
    fontSize: 18,
    fontWeight: '800',
    textAlign: 'center'
  },
  shareButtonSocialNetworkContainer: {
    flexDirection: 'row',
    alignItems:'center',
    padding: 10,
    paddingTop: 20
  },
  shareButtonSocialNetwork: {
    width: '33%',
    alignItems:'center',
    justifyContent:'center'
  }

}
