import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Text, View, Image, Modal, Linking, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import styles from './styles'

export class DetailNewsPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentImage: {},
      currentImageIndex: 0,
      shareModalVisible: false,
    }
  }

  async componentWillMount(){
    this.setState({currentImage: this.props.navigation.state.params.image})
  }

  setShareModalVisible(visible) {
    this.setState({shareModalVisible: visible});
  }

  renderShareModal(imageName, imageUrl) {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.shareModalVisible}
        onRequestClose={() => {
          alert('Modal has been closed.');
        }}>
        <View style={styles.shareModalWindow}>
          <View>
            <TouchableHighlight
              style={styles.closeModalButtonContainer}
              onPress={() => {
                this.setShareModalVisible(!this.state.shareModalVisible);
              }}
            >
              <FontAwesome
                name='times'
                size={25}
                color="#d63031"
              />
            </TouchableHighlight>

            <Text style={styles.shareModalWindowTitle}>
              Share Image Url
            </Text>


            <View style={styles.shareButtonSocialNetworkContainer}>
              <TouchableHighlight
                style={styles.shareButtonSocialNetwork}
                onPress={() => {
                  Linking.openURL(`http://twitter.com/share?text=${imageName}&url=${imageUrl}`)
                }}
              >
                <FontAwesome
                  name='twitter-square'
                  size={50}
                  color="#1dcaff"
                />
              </TouchableHighlight>

              <TouchableHighlight
                style={styles.shareButtonSocialNetwork}
                onPress={() => {
                  Linking.openURL(`http://www.facebook.com/sharer.php?u=${imageUrl}[title]=${imageName}`)
                }}
              >
                <FontAwesome
                  name='facebook-square'
                  size={50}
                  color="#3b5998"
                />
              </TouchableHighlight>

              <TouchableHighlight
                style={styles.shareButtonSocialNetwork}
                onPress={() => {
                  Linking.openURL(`http://pinterest.com/pin/create/button/?url=${imageUrl}&description=${imageName}`)
                }}
              >
                <FontAwesome
                  name='pinterest-square'
                  size={50}
                  color="#bd081c"
                />
              </TouchableHighlight>
            </View>

          </View>
        </View>
      </Modal>
    )
  }

  render() {
    const { currentImage } = this.state

    return (
      <View style={styles.imageInnerContainer}>
        {this.renderShareModal(currentImage.name, currentImage.image_url[0])}
        <Image
          style={styles.imageInner}
          source={{uri: currentImage.image_url[0]}}
          resizeMode={'cover'}
        />


        <View style={styles.overlayContainer}>
          <View style={styles.overlayAdditionalInfo}>
            <Text style={styles.overlayTitle}>{currentImage.name}</Text>
            <Text style={styles.overlayMoreInfo}>{currentImage.user.fullname}</Text>
            <Text style={styles.overlayMoreInfo}>{currentImage.camera}</Text>
          </View>

          <TouchableHighlight
            style={styles.shareButton}
            onPress={() => {
              this.setShareModalVisible(true);
            }}
          >
            <FontAwesome
              name='share-alt'
              size={20}
              color="#ffffff"
            />
          </TouchableHighlight>
        </View>
      </View>

    );
  }
}

const mapStateToProps = (state) => ({
  images: state.gallery.list
})

DetailNewsPage.propTypes = {
  navigation: PropTypes.object,
  getNews: PropTypes.func
};

export default connect(
  mapStateToProps
)(DetailNewsPage)
