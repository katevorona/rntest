import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, Image, FlatList, TouchableHighlight } from 'react-native';
import PropTypes from 'prop-types';

import { getImages } from "../../actions/gallery";
import styles from './styles'

const LOADER_IMAGE = require('../../src/images/loader.gif')

class HomePage extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      images: [],
      currentPage: 1,
      currentPageImages: []
    }

    this.onEndReached = this.onEndReached.bind(this)
    this.newImagesList = this.newImagesList.bind(this)
  }

  componentWillMount() {
    this.props.getImages()
  }

  async componentWillReceiveProps(nextProps) {
    if(nextProps.images.length > 0) {
      console.log(nextProps.images)
      this.setState({ images: this.newImagesList(nextProps.images), loading: nextProps.loading, currentPage: nextProps.currentPage})
    }
  }

  newImagesList (newImages) {
    const { images } = this.state
    const existedImages = Object.assign([], images)
    const elem = existedImages.find(image => image.id === newImages[0].id)
    if(!elem) {
      return existedImages.concat(newImages)
    }
    return existedImages
  }

  renderItem = ({item}) => {
    const { navigate } = this.props.navigation;
    return (
      <TouchableHighlight
        style={styles.imageItem}
        onPress={() => navigate('DetailPage', { image: item })}
      >
        <Image
          style={styles.imagePreview}
          source={{uri: item.image_url[1]}}
          resizeMode={'cover'}
        />
      </TouchableHighlight>
    );
  }

  keyExtractor = (item, index) => item.id.toString();

  onEndReached() {
    const newPage = this.state.currentPage+1;
    this.props.getImages(newPage)
  }

  render() {
    const { loading, images } = this.state;

    return (
      <View>
        <FlatList
            data={images}
            contentContainerStyle={styles.list}
            numColumns={2}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            onEndReached={this.onEndReached}
        />
        {loading ?
          <View style={styles.loaderContainer}>
            <Image
              style={styles.loader}
              source={LOADER_IMAGE}
              resizeMode={'contain'}
            />
          </View>
          : null
        }
      </View>

    );
  }
}

const mapStateToProps = (state) => ({
  images: state.gallery.list,
  currentPage: state.gallery.currentPage,
  loading: state.gallery.loading
})

const mapDispatchToProps = dispatch => ({
  getImages: (page) => dispatch(getImages(page)),
})

HomePage.propTypes = {
  navigation: PropTypes.object,
  getImages: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage)
