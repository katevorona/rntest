const initialState = {
  list: [],
  currentPage: 1,
  loading: false
}

export default function inventory(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_GALLERY':
      return Object.assign({}, state, {
        list: [],
        loading: true
      })
    case 'LOAD_GALLERY_FAIL': {
      return Object.assign({}, state, {
        list: [],
        loading: false
      })
    }
    case 'LOAD_GALLERY_SUCCESS': {
      return Object.assign({}, state, {
        list: action.data,
        currentPage: action.currentPage,
        loading: false
      })
    }

    default: {
      return state
    }
  }
}
