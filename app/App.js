/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Router from './routers/router';
import configureStore from './store/configureStore'

const store = configureStore();

export default class SimpleApp extends Component {
  render () {
    return (
      <Provider store={store}>
        <Router/>
      </Provider>
    );
  }
}
