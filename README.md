# RNTest

## Install all dependencies
Run `npm install`

## Run Application
Run `react-native run-ios`

If you have some errors while running code above, visit this page please https://facebook.github.io/react-native/docs/getting-started.html and do everything according documentation

## Notes
I checked application only on ios (X-Code emulator). I haven't enough time to check it on Android also. So i can't be sure it works fine on both platform
