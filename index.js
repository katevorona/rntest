import { AppRegistry } from 'react-native';
import App from './app/App';
import { YellowBox } from 'react-native';

// sorry guys, for this one. It's problem in RN itself in version ^0.55.2.
// It just made me crazy. That's why i decided to hide it
// https://github.com/react-navigation/react-navigation/issues/3956
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

AppRegistry.registerComponent('RNTest', () => App);
